from django.conf.urls import url

from mapviewport import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^locations/add/$', views.add_location, name='add_location'),
    url(r'^locations/clear/$', views.clear_locations, name='clear_locations'),
]