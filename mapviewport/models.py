from django.db import models


class MapLoc(models.Model):
	maploc_id   	= models.AutoField(primary_key=True)
	maploc_lat 		= models.FloatField('Lat')
	maploc_lng 		= models.FloatField('Lng')
	maploc_address 	= models.CharField('Address',unique=True, max_length=255) #does not allow an address to be repeated


	