var map;

function add_marker(lat_value, lng_value, address){
	//add the marker and the entry on the table
	var html = '<tr class="address">';
	html 	+= '<td>'+lat_value+'</td>';
	html 	+= '<td>'+lng_value+'</td>';
	html 	+= '<td>'+address+'</td>';
	html 	+= '</tr>';

	$('#address-table').append(html);

	map.addMarker({
	  lat: lat_value,
	  lng: lng_value
	});
};

function clear_map(){
	//remove all markers from the map and the table
	map.removeMarkers();
	$('tr.address').remove();
};

function add_marker_to_db(lat_value, lng_value, address){

	//add the marker details to the django database, and fusion table
	$.ajax({
		cache: false,
		dataType: "json",
		url: '/locations/add/',
		data: {lat: lat_value, lng: lng_value, address: address},
		contentType: "application/json; charset=utf-8",
		success: function(res){
			if(res.result=='ok')
				add_marker(lat_value, lng_value, address);
			else
				alert(res.msg);
			
		}
	}).fail(function(xhr){
		alert(xhr.status+" "+xhr.statusText+": "+xhr.responseText);
	}).always(function() {
		$('.form').removeClass('loading');
	});
};

function clear_db(){
	//Clear all the registers from django database and from the fusion table
	$('.form').addClass('loading');
	$.ajax({
		cache: false,
		dataType: "json",
		url: '/locations/clear/',
		contentType: "application/json; charset=utf-8",
		success: function(res){
			if(res.result=='ok')
				clear_map(); //if the request was successfully clear all the markers 
			else
				alert(res.msg);
		}
	}).fail(function(xhr){
		alert(xhr.status+" "+xhr.statusText+": "+xhr.responseText);
	}).always(function() {
		$('.form').removeClass('loading');
	});
};


$(document).ready(function(){
	//initiate the map
	map = new GMaps({
		div: '#map',
		lat: 38.7436214,
		lng: -9.1952231
	});

	//load all the markers from the fusion table.
	$('.form').addClass('loading');
	$.ajax({
		url: "https://www.googleapis.com/fusiontables/v2/query",
		type: "GET",
		cache:false,
		data: {
			sql: "SELECT Location, Address FROM " + fusion_table,
			key: api_key
		},
		success: function (data) {
			if(data.rows)
			for(var i=0; i<data.rows.length; i++){
				var coords = data.rows[i][0].split(',');
				var address = data.rows[i][1];
				add_marker(coords[0], coords[1], address);
			};
		}
	}).always(function() {
		$('.form').removeClass('loading');
	});


  //set the map click event
  GMaps.on('click', map.map, function(event) {
  	$('.form').addClass('loading');

	var lat_value = event.latLng.lat();
	var lng_value = event.latLng.lng();

	//search for the location address
	GMaps.geocode({
	  lat: lat_value,
	  lng: lng_value,
	  callback: function(results, status) {
		if (status == 'OK') {
			var obj = results[0];
			
			//validates if the marker currespond to a residential area, if so add the address to the database
			if(obj.types[0]=='street_address') 
				add_marker_to_db(lat_value, lng_value, obj.formatted_address)
			else
				$('.form').removeClass('loading');
		};
	  }
	});

  });
});