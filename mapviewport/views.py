import simplejson
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render_to_response
from mapviewport.models import MapLoc
from mapviewport.utils.fusion_tables import fusion_table_query


def index(request):
	data = { 
		'API_KEY':settings.FUSIONTABLES_API_KEY,
		'FUSIONTABLES_TABLE_ID': settings.FUSIONTABLES_TABLE_ID
	}
	return render_to_response('index.html', data)


def add_location(request):

	location = MapLoc()
	location.maploc_lat = request.GET['lat']
	location.maploc_lng = request.GET['lng']
	location.maploc_address = request.GET['address']
	try:
		#save the location in the database
		location.save() #if the register is duplicate it will raise an exception
		#save the location to the fusion table
		query = "INSERT INTO {3} (Location, Address) VALUES ('{0},{1}', '{2}')".format(
			location.maploc_lat, location.maploc_lng, location.maploc_address,
			settings.FUSIONTABLES_TABLE_ID
		)
		fusion_table_query(query, post=True)
		data = {'result':'ok'  }
	except Exception as e:
		data = {'result':'failed', 'msg': str(e)  }
		
	return HttpResponse(simplejson.dumps(data), "application/json")
	
	

def clear_locations(request):
	#delete all locations from the database
	MapLoc.objects.all().delete()
	#delete all locations from the fusion tables
	fusion_table_query("DELETE FROM {0}".format(settings.FUSIONTABLES_TABLE_ID), post=True)
	return HttpResponse(simplejson.dumps({'result':'ok'}), "application/json")