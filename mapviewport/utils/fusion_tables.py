import os, httplib2, argparse
from urllib.parse import urlencode
from oauth2client import client, GOOGLE_TOKEN_URI, GOOGLE_REVOKE_URI, tools
from oauth2client.file import Storage
from datetime import datetime
from django.conf import settings

SCOPES 			= 'https://www.googleapis.com/auth/fusiontables'
FUSION_API_URL 	= 'https://www.googleapis.com/fusiontables/v2/query'

def get_authorized_http():
	"""
	Gets valid user credentials from storage.
	If nothing has been stored, or if the stored credentials are invalid,
	the OAuth2 flow is completed to obtain the new credentials.
	Returns: an authenticated http channel.
	"""
	http = httplib2.Http()
	credential_path = 'google-credentials.json'
	store 		= Storage(credential_path)
	credentials = store.get()
	if not credentials or credentials.invalid:
		parser = argparse.ArgumentParser(parents=[tools.argparser])
		flags = parser.parse_args()

		flow 			= client.flow_from_clientsecrets(settings.FUSIONTABLES_CLIENT_SECRET_FILE, SCOPES)
		flow.user_agent = settings.FUSIONTABLES_APPLICATION_NAME
		credentials 	= tools.run_flow(flow, store, flags)
	
	# Renew the credentials if it expired
	if credentials.token_expiry<datetime.now(): credentials.refresh(http)
	credentials.authorize(http)
	return http


def fusion_table_query(query, post=False):
	"""
	Execute a query to the fusion tables REST api
	Returns: the http response
	"""
	http 	= get_authorized_http()
	query 	= {'sql': query, 'key':settings.FUSIONTABLES_API_KEY}
	URL 	= '{0}?{1}'.format(FUSION_API_URL, urlencode(query))
	return http.request(URL, method="POST", body=urlencode(query) if post else None)
