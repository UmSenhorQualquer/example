from django.apps import AppConfig


class MapviewportConfig(AppConfig):
    name = 'mapviewport'
