# Viewport map example #

This is a django simple application to stores maps coordenates defined by the user.

![Printscreen](https://bytebucket.org/UmSenhorQualquer/example/raw/a7003b7b5a9983e091a219827fbd65176701ac32/docs/printscreen.png)

### Development enviroment ###

* Windows 10
* Python 3.4

### Configure the Google OAuth2 to access the Fusion Tables ###

1. Create a fusion table with 2 columns: Location (type location), Address (type text).
1. Set in django settings.py file the variables:
	* FUSIONTABLES_TABLE_ID = '[Fusion table ID]'
	* FUSIONTABLES_API_KEY  = '[GOOGLE API KEY]'
	* **Note:** Get an API KEY using the link: https://console.developers.google.com/apis/credentials?project=_
2. Create the file google-oauth-conf.json in the django app directory with the format next format:
```json
{  
	"installed": {  
        "client_id": "[Google API Client ID]",  
        "client_secret": "[Google API Secret key]",  
        "redirect_uris": ["http://localhost:8080"],  
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",  
        "token_uri": "https://accounts.google.com/o/oauth2/token"  
      }  
}
```
3. Configure the google-oauth-conf.json with the right credentials.
3. Execute the script setup_google_oauth3.py to give the application write access to the fusion table.

### Quick install & run ###

  virtualenv .ve && source .ve/bin/activate && pip install -r requirements.txt && python setup_google_oauth2.py && python3 manage.py migrate && python3 manage.py runserver
