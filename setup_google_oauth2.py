#!/usr/bin/python3

import os, django
os.environ['DJANGO_SETTINGS_MODULE'] = 'example.settings'
django.setup()

from mapviewport.utils.fusion_tables import get_authorized_http

if __name__ == '__main__':  
	get_authorized_http()