#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__      = "Ricardo Ribeiro"
__credits__     = ["Ricardo Ribeiro"]
__license__     = "MIT"
__version__     = '0'
__maintainer__  = "Ricardo Ribeiro"
__email__       = "ricardojvr@gmail.com"
__status__      = "Production"

from setuptools import setup
import os, fnmatch


def find_files(package_name,directory, pattern):
    for root, dirs, files in os.walk(os.path.join(package_name, directory)):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root[len(package_name)+1:], basename)
                yield filename

setup(

	name				='mapviewport',
	version 			=__version__,
	description 		="App to select locations on google maps",
	author  			=__maintainer__,
	author_email		=__email__,
	license 			=__license__,
	url 				='https://github.com/UmSenhorQualquer/pyforms',
	include_package_data=True,
	packages=['mapviewport'],
	package_data={'mapviewport':list( find_files('mapviewport','static/js/', '*.*') )},
	install_requires=[
		"django==1.9",
		"simplejson"
	],
)